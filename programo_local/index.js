const csvtojson = require('csvtojson');
const mysql = require('mysql2');

//db credentials

const hostname = 'localhost',
      username = 'root',
      password = ''  ,
      database = 'acamicadb'

//establish connection to the db
let con = mysql.createConnection({
    host: hostname,
    user: username,
    password: password,
    database: database,
});

con.connect((err) => {

    if(err) return console.error('error' + err.message);

    con.query("DROP TABLE students",
    (err, drop) => {
        //query to create table "sample"
        var createStatement = "CREATE TABLE students(Name char(50), " + 
        "Email char(50), age int, city char(30))"

        //creating table "sample"
        con.query(createStatement, (err, drop) => {
            if(err)
                console.log('ERROR: ', err);
        });
    });
});


//CSV File name

const fileName = "F:/Google Drive/acamica/sprint 2/vq/data.csv";

csvtojson().fromFile(fileName).then(source => {
    //fetching the data from each row
    //and inserting to the table "sample"

    for (let i = 0; i < source.length; i++) {
        
        var Name = source[i]["Name"],
            Email = source[i]["Email"],
            Age = source[i]["Age"],
            City = source[i]["City"]
        
        var insertStatement = 
        `INSERT INTO students values(?, ?, ?, ?)`;
        var items = [Name, Email, Age, City];

        //Inserting data of current row
        //into database
        con.query(insertStatement, items,
            (err, results, fields) => {
                if(err) {
                    console.log("No se puede insertar el registro en el item", i + 1);
                }
        });        
    }
    console.log("Todos los items fueron almacenados satisfactoriamente");
});
