/*---querys join--*/
use acamicadb;
​
/**INNER JOINS*/
/*Este tipo de unión te ayuda a combinar varias tablas, y te devuelve únicamente los datos que estén disponibles en todas las tablas a la vez*/
SELECT *
FROM
personas AS P 
INNER JOIN punto_contacto as PC 
	ON P.id = PC.personas_id 
    
INNER JOIN tipo_contacto as TC
	on TC.id = PC.tipo_contacto_id
    
limit 2;
    
/**LEFT JOINS*/
​
/*
el left join devuelve todos los resultados que coincidan en la primera tabla, con los datos que tenga de la segunda.
En el caso de que falte algún dato, devolverá un valor null en lugar del dato, pero seguiremos teniendo el valor de la primera tabla
*/
use acamicadb;
SELECT *
FROM
personas AS P 
LEFT JOIN punto_contacto as PC 
	ON P.id = PC.personas_id 
    
LEFT JOIN tipo_contacto as TC
	on TC.id = PC.tipo_contacto_id;
    
    
/*
 RIGHT JOIN es uno de los join que se pueden implementar en un SELECT. Permite obtener información de dos tablas, 
 pero siempre considerando que los registros resultantes deben estar en la segunda.
*/
use acamicadb;
SELECT *
FROM
personas AS P 
RIGHT JOIN punto_contacto as PC 
	ON P.id = PC.personas_id 
    
RIGHT JOIN tipo_contacto as TC
	on TC.id = PC.tipo_contacto_id;
    
    
    
/*
 full outer joins  NO EXISTEN EN MySQL, PERO SE EMULAN CONSULTANDO DERECHA E IZQ Y HACIENDO UNA UNION
 https://stackoverflow.com/questions/4796872/how-can-i-do-a-full-outer-join-in-mysql 
*/
use acamicadb;
SELECT *
FROM
personas AS P 
LEFT OUTER JOIN punto_contacto as PC 
	ON P.id = PC.personas_id 
    
LEFT OUTER JOIN tipo_contacto as TC
	on TC.id = PC.tipo_contacto_id
    
UNION
​
SELECT *
FROM
personas AS P 
RIGHT OUTER JOIN punto_contacto as PC 
	ON P.id = PC.personas_id 
    
RIGHT OUTER JOIN tipo_contacto as TC
	on TC.id = PC.tipo_contacto_id
