const { Sequelize } = require('sequelize');
const bandaModelo = require('./models/banda');
const cancionModelo = require('./models/cancion');
const albumModelo = require('./models/album');

//config de bd sequelize
const sequelize = new Sequelize('bandas', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
});

validar_conexion();

const Banda = bandaModelo(sequelize, Sequelize);
const Cancion = cancionModelo(sequelize, Sequelize);
const Album = albumModelo(sequelize, Sequelize);

sequelize.sync({ force:false })
    .then(() => {
        console.log('Tablas sincronizadas');
    })

async function validar_conexion() {
    try {
        await sequelize.authenticate();
        console.log('Conexión establecida correctamente');
    } catch(error) {
        console.error('Error al conectarse a la bd: ', error);
    }
}

module.exports = {
    Banda,
    Cancion,
    Album,
};