const router = require('Express').Router();
const { bandas, Banda } = require('../../db');

router.get('/home', (req, res) => {    
    console.log('estamos en el home de bandas');
    res.send('Hola Bandas');
});


//Listar bandas
router.get('/', async (req, res) => {
    const bandas = await Banda.findAll();
    console.log('estamos en el get para listar bandas');
    res.json(bandas);
});


//crear banda
router.post('/', async (req, res) => {
    const banda = await Banda.create(req.body);
    res.json(banda);
});


//actualizar banda
router.put('/:bandaId', async (req, res) => {
    await Banda.update(req.body, {
        where:{id:req.params.bandaId}
    });
    
    res.json('banda modificada');
});


//borrar banda
router.delete('/:bandaId', async (req, res) => {

    await Banda.destroy({
        where: {
            id:req.params.bandaId
        }
    })
    .then(function (deletedRecord) {
        if(deletedRecord === 1) {
            res.status(200).json({message:"banda no encontrada"})
        }
    })
    .catch(function (error) {
        res.status(200).json(error);
    });
});


module.exports = router;