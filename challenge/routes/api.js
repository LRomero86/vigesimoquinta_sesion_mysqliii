const router = require('express').Router();
const apiBandas = require('./api/bandas');

router.use('/bandas', apiBandas);

module.exports = router;