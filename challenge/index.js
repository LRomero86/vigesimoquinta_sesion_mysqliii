const express = require('express');
const app = express();

//llamamos a la bd

require('./db');

const apiRouter = require('./routes/api');

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

app.use('/api', apiRouter);

app.listen(3000, () => {
    console.log('Listening in port 3000');
});