module.exports = (sequelize, type) => {
    
    return sequelize.define('albumes', {
        id:{
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nombre_album: type.STRING,
        banda_id: type.INTEGER,
        fecha_publicacion: type.DATE,
    });
}