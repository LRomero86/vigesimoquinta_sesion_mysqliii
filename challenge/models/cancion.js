module.exports = (sequelize, type) => {

    return sequelize.define('canciones', {
        id:{
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nombre: type.STRING,
        duracion: type.INTEGER,
        album_id: type.INTEGER,
        banda_id: type.INTEGER,
        fecha_publicacion: type.DATE,
    });
}