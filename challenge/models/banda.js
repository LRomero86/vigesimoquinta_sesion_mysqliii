module.exports = (sequelize, type) => {
    
    return sequelize.define('bandas', {
        id:{
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nombre: type.STRING,
        integrantes: type.INTEGER,
        fecha_inicio: type.DATE,
        fecha_separacion: type.DATE,
        pais: type.INTEGER,
    });
}